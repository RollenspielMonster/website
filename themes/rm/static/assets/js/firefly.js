// URL zur JSON-Datei
const url = 'transactions.json';

// Funktion zum Abrufen der JSON-Daten
async function getTransactions() {
  try {
    const response = await fetch(url);
    const data = await response.json();
    return data.data.flatMap(item => item.attributes.transactions);
  } catch (error) {
    console.log('Fehler beim Abrufen der Daten:', error);
  }
}

// Funktion zum Zusammenfassen der Transaktionen nach Monat und Typ
function summarizeTransactions(transactions) {
  const summary = {};

  transactions.forEach(transaction => {
    const date = new Date(transaction.date);
    const year = date.getFullYear();
    const month = date.getMonth() + 1; // Monate in JavaScript sind 0-basiert

    // Nur Transaktionen im aktuellen Jahr berücksichtigen
    const currentYear = new Date().getFullYear();
    if (year !== currentYear) {
      return;
    }

    if (!summary[month]) {
      summary[month] = {
        deposits: 0,
        withdrawals: 0
      };
    }

    if (transaction.type === 'deposit') {
      summary[month].deposits += parseFloat(transaction.amount);
    } else if (transaction.type === 'withdrawal') {
      summary[month].withdrawals += parseFloat(transaction.amount);
    }
  });

  return summary;
}

// Funktion zum Ausgeben der Zusammenfassung für jeden Monat
function getMonthlySummary(summary) {
  const monthlySummary = {
    deposits: [],
    withdrawals: []
  };

  for (let month = 1; month <= 12; month++) {
    if (summary[month]) {
      const deposits = parseFloat(summary[month].deposits.toFixed(2));
      const withdrawals = parseFloat(summary[month].withdrawals.toFixed(2));

      monthlySummary.deposits.push(deposits);
      monthlySummary.withdrawals.push(withdrawals);
    } else {
      monthlySummary.deposits.push(0);
      monthlySummary.withdrawals.push(0);
    }
  }

  return monthlySummary;
}

// Hauptfunktion zum Abrufen der Daten und Erstellen der Zusammenfassung
async function calculateMonthlySummary() {
  const transactions = await getTransactions();

  if (!transactions) {
    return;
  }

  const summary = summarizeTransactions(transactions);
  const monthlySummary = getMonthlySummary(summary);

  const ctx = document.getElementById("myChart").getContext("2d");
  new Chart(ctx, {
    type: "bar",
    data: {
      labels: ["Januar", "Februar", "März", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember"],
      datasets: [
        {
          label: "Einnahmen",
          backgroundColor: "rgba(25, 135, 84, 0.7)",
          data: monthlySummary.deposits,
        },
        {
          label: "Ausgaben",
          backgroundColor: "rgba(220, 53, 69, 0.7)",
          data: monthlySummary.withdrawals,
        },
      ],
    },
    options: {
      // Konfigurationsoptionen für das Diagramm
    },
  });
}

// Aufruf der Hauptfunktion
calculateMonthlySummary();
