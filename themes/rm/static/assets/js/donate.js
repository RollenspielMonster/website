document.addEventListener("DOMContentLoaded", function () {
  // Initialen QR-Code generieren
  generateQRCode();

  document.getElementById("amount").addEventListener("input", handleAmountInput);
  document.getElementById("notiz").addEventListener("input", generateQRCode);
});

function handleAmountInput() {
  var maxAmount = 100;
  var amountInput = document.getElementById("amount");
  var enteredAmount = parseFloat(amountInput.value);

  if (isNaN(enteredAmount) || enteredAmount > maxAmount) {
    // Wenn die Eingabe keine Zahl ist oder über 100 liegt, setze den Wert auf 100
    amountInput.value = maxAmount;
  }

  generateQRCode();
}

function setAmount(amount) {
  document.getElementById("amount").value = amount;
  generateQRCode();
}

function generateQRCode() {
  var amount = document.getElementById("amount").value;
  var notiz = document.getElementById("notiz").value;
  var beneficiaryName = "RollenspielMonster";
  var iban = "NL67BUNQ2290486728";
  var bic = "BUNQNL2A";
  var purpose = "Freiwillige finanzielle Unterstuetzung";
  amount += ".00";

  // Wenn eine Notiz angegeben wurde, füge ihn dem Verwendungszweck hinzu
  if (notiz.trim() !== "") {
    purpose += " - " + notiz.trim();
  }

  // EPC-QR-Code generieren
  // https://wiki.adminforge.de/wiki/EPC-QR-Code?lang=de
  var sepa = "BCD\n" + "002\n" + "1\n" + "SCT\n" + bic + "\n" + beneficiaryName + "\n" + iban + "\n" + "EUR" + amount + "\n" + "\n" + "\n" + purpose;

  var qrCodeContainer = document.getElementById("qrCodeContainer");
  qrCodeContainer.innerHTML = ""; // Leere das Container-Element

  var qrcode = new QRCode(qrCodeContainer, {
    text: sepa,
    width: 256,
    height: 256,
  });

  // Bankdaten anzeigen
  var bankDataContainer = document.getElementById("bankDataContainer");
  bankDataContainer.innerHTML =
    "<p><strong>Zahlungsempfängers:</strong> " +
    beneficiaryName +
    "</p>" +
    "<p><strong>IBAN:</strong> " +
    iban +
    "</p>" +
    "<p><strong>BIC:</strong> " +
    bic +
    "</p>" +
    "<p><strong>Zahlungsbetrag:</strong> " +
    amount + " €" +
    "</p>" +
    "<p><strong>Verwendungszweck:</strong> " +
    purpose +
    "</p>";
}
