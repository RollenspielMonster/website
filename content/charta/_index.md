---
title: Charta
date: 2021-04-05T00:00:00.000Z
lastmod: 2024-02-15T07:27:57.467Z
tags:
  - ""
categories:
  - ""
slug: charta
footline: Spezielle Regeln zu einzelnen Diensten
---


## Präambel

<b class="firstpart">Rollenspiel</b><b class="lastpart">Monster</b> stellt einige Dienste zur Verfügung. Diese Dienste bringen Menschen mit unterschiedlichen Hintergründen und Bildungsvoraussetzungen zusammen. Wir träumen von einer Welt der Einhörner und Regenbögen, in der sich alle in Ruhe austauschen und voneinander lernen können. Dennoch sind wir uns bewusst, dass es diese perfekte Welt nicht gibt und wir einen Rahmen für die Interaktion innerhalb der von uns bereitgestellten Dienste vorgeben müssen.
Damit dieser Austausch in einem solchen Rahmen stattfinden kann, definiert diese Charta, welche Verhaltensweisen wir fördern und welche wir ablehnen. Die Beteiligung aller ist notwendig: Ohne Berichterstattung können wir oftmals nicht viel tun, und jeder muss diese Regeln kennen, um sie anzuwenden. Wir möchten euch daher ermutigen, diese Charta so weit wie möglich zu verbreiten.

## Umfang der Anwendung

Diese Charta gilt für alle Personen, die sich innerhalb von <b class="firstpart">Rollenspiel</b><b class="lastpart">Monster</b> bewegen, insbesondere auf unseren Websites und Dienstleistungen.

## Sprache

Hauptsprachen sind Deutsch und Englisch, daher bitten wir alle Posts auch nur in diesen Sprachen zu verfassen. Anderen Sprachen können von uns [nicht moderiert werden](https://santaclaraprinciples.org/#3-cultural-competence), daher werden wir diese entfernen.

## Rechtlicher Rahmen

Wir unterstehen der deutschen Rechtssprechung und alle Verstöße, die wir auf unseren Medien feststellen, können rechtliche Folgen haben. Außerdem werden diese moderiert, sodass sie nicht mehr öffentlich erscheinen.
Dies betrifft unter anderem (nicht abschließende Aufzählung):

- Beleidigen
- Verleumdung
- Diskriminierung
- Bedrohungen
- Belästigung
- Eingriff in die Privatsphäre
- Hassrede
- Der Jugendschutz, insbesondere im Hinblick auf das Vorhandensein von pornografischen Inhalten sowie Gewaltdarstellungen

## Andere Verhaltensweisen, die moderiert werden

Die folgenden Verhaltensweisen werden wir ebenfalls nicht akzeptieren, da sie ebenso kritisch sind:

- Senden von aggressiven Nachrichten an eine Person oder Gruppe, egal aus welchem Grund. Wenn du beleidigt wirst, [melde](/contact) es, beleidige nicht zurück.
- Enthüllung der privaten Informationen einer Person. Die Offenlegung von Pseudonymen und anderen Details der realen Identität ist eine persönliche und individuelle Entscheidung, die nicht aufgezwungen werden kann.
- Belästigung einer Person, wenn diese dich gebeten hat, aufzuhören. Respektiere dies, »Nein« heißt »Nein«! Lasse Menschen in Ruhe, die nicht mit dir reden wollen.
- Verwende niemals herabsetzende Sprache zu Fragen des Geschlechts, der sexuellen Orientierung, des körperlichen Aussehens, der Kultur, der körperlichen oder geistigen Verfassung usw. und ganz allgemein zu allem, was systematische Unterdrückung aufrechterhält. Wenn du dachtest, deine Formulierungen seien nur scherzhaft gemeint, dann solltest du wissen, dass wir bei diesen Themen keinen Humor haben und dies nicht tolerieren werden. Frage dich im Zweifelsfall vorher, ob die Menschen, die du ansprichst, mit dir über deinen Scherz lachen werden.
- Automatisch erzeugte Meldungen werden nur toleriert, wenn sie gut umgesetzt sind.
- Intensive negative Emotionen (insbesondere Wut) teilen, vorwiegend gegenüber einer Person. Soziale Medien sind kein geeigneter Ort, um Rechnungen zu begleichen oder psychologische Hilfe zu suchen. Verlasse den PC und die Tastatur oder leg dein Tablet oder Smartphone beiseite, atme durch. Um intensive Emotionen auszudrücken, gibt es geeignetere Orte und Medien, diese lautstark mitzuteilen. Wenn die erste Wut verflogen ist, kannst du (falls nötig) in aller Ruhe zurückkehren und deine Bedürfnisse und Grenzen darlegen. Wir wissen selbst, dass dies oft schwer ist.
- Teilnahme an Massenbelästigungen, Anstiftung zu allgemeiner Verunglimpfung und Horten von Nachrichten (auch Dogpiling genannt). Für dich mag es nur eine Nachricht sein, aber für die Person, die sie empfängt, ist es eine Nachricht zu viel und kann verletzen.
- Boshafte Verunglimpfung einer Person, eines Verhaltens, eines Glaubens, einer Praxis. Du hast das Recht auf Ablehnung, aber wir wollen dein Urteil nicht lesen.
- Wir tolerieren nicht das Streuen von Fehlinformationen/Verschwörungsmythen in öffentlichen Räumen. Beispielsweise: Corona sei eine Lüge, Impfungen beinhalten Chips, Chemtrails, die Erde sei rund - äh - flach, das fliegende Spaghetti Monster würde nicht existieren oder Ähnliches.

### Arten von Konten, die moderiert werden

Folgende Konten sind bei uns unerwünscht und werden gelöscht:

- Bots: wenn die Erstellung automatisiert wirkt.
- Werbung: wenn der Account für Werbezwecke erstellt wurde (unabhängig von dem Veröffentlichungsort des Textes).

Falls ihr ein berechtigtes Interesse habt, einen Bot zu betreiben, [kontaktiert](/contact) uns bitte vorher.

## Menschen sind nicht ihre Verhaltensweisen

Wir unterscheiden zwischen Menschen und Verhalten. Wir werden Verhalten, das gegen unseren Verhaltenskodex verstößt, wie unter [Moderationsmaßnahmen](/charta/#moderationsma%C3%9Fnahmen) beschrieben, moderieren, aber wir werden Personen nicht unter dem Vorwand ihrer Meinung nach dem ersten Verstoß verbannen.

Wir werden jedoch jeden verbannen, der wiederholt gegen unsere Richtlinien verstößt und unsere Moderationsteams mobilisiert hat: Wir wollen mit Leuten sprechen, die in der Lage sind, unsere Charta zu verstehen und/oder zu respektieren.

## Werkzeuge zu Ihrer Verfügung

Wir bitten euch, wachsam zu sein. Dies soll ein sicherer Ort für alle sein. Helft uns, die uns zur Verfügung stehenden Werkzeuge zu nutzen, um unsere Dienste angenehm zu gestalten:

- Wenn du ein Problem mit jemandem hast oder dich in einer Situation unwohl fühlst, benutze dies nicht als Rechtfertigung für Aggressionen. Du hast je nach Dienst unterschiedliche Optionen, unter anderem:
  - das Gespräch beenden
  - das unerwünschte Konto sperren
  - die gesamte Instanz sperren, mit der dieses Konto verbunden ist
  - das Konto an die Moderation [melden](/contact)
- Wenn du von jemandem blockiert wirst, höre auf seinen oder ihren Wunsch und versuche nicht, diese Person auf anderem Wege zu kontaktieren, denn es liegt an ihr, dies zu tun, wenn sie dazu Lust hat.
- Wenn du ein Verhalten bemerkst, welches gegen den Verhaltenskodex verstößt und zu Problemen führt, [melde](/contact) es der Moderation, damit diese Maßnahmen ergreifen kann. Meldungen ermöglichen es uns, Menschen in Not zu helfen und unerwünschtes Verhalten zu unterbinden und unsere Community zu einem sicheren Ort für alle zu machen.
- Wenn es nicht sicher ist, dass eine Person Probleme hat, aber die Verhaltensweisen ihr gegenüber fragwürdig sind, führe ein Gespräch über diese Verhaltensweisen mit jeder der betroffenen Personen in einer respektvollen Art und Weise, um Ihre gemeinsame Wahrnehmung der fraglichen Verhaltensweisen zu hinterfragen und zu ändern.

## Moderationsmaßnahmen

Wenn uns ein Verhalten auffällt oder wir auf ein Verhalten aufmerksam gemacht werden, welches gegen diese Charta verstößt, werden wir nach bestem Wissen und Gewissen und den Kräften unserer Freiwilligen handeln. Um uns die Arbeit zu erleichtern, ist es **dringend erforderlich**, dass ihr eure **Meldungen begründet**, soweit möglich. Ohne diese Begründungen können wir deine Gedankengänge nicht verstehen und werden die Meldung gegebenenfalls einfach schließen.

In einigen Fällen werden wir versuchen, uns die Zeit zu nehmen, mit den Betroffenen zu sprechen. Es ist aber auch möglich, dass wir einige dieser Aktionen ohne weitere Begründung durchführen:

- Entfernung von Inhalten (Nachrichten, Videos, Bilder usw.)
- Änderung (oder Änderungswunsch) des gesamten oder eines Teils des Inhalts (Inhaltswarnung, Löschung eines Teils)
- Sperrung des Kontos oder der Instanz im Falle der Nichteinhaltung der [Nutzungsbedingungen](/terms/) (missbräuchliche Nutzung der Dienste) oder der Nichteinhaltung dieser Charta.

Im Rahmen des Fediverse (Mastodon, Mobilizon, PeerTube …) können wir die auf unsere Instanz weitergeleiteten problematischen Inhalte ausblenden oder sogar problematische Instanzen blockieren (beides wird als letzte mögliche Maßnahme angesehen), aber nicht direkt auf die anderen Instanzen einwirken.

Die Moderation wird von Freiwilligen (und Menschen!) durchgeführt, die nicht immer die Zeit und Energie haben, dir Begründungen zu geben. Wenn du glaubst, dass du zu Unrecht moderiert oder verbannt wurdest, wende dich an uns: Wir sind manchmal ungerecht, ohne dies zu wollen; wir sind meist Menschen und daher fehlbar. So ist das nun mal. Aber mit uns kann geredet werden.

## Im Falle einer Unstimmigkeit

Wenn dir die Art und Weise, wie einer unserer Dienste verwaltet wird, nicht gefällt:

- kann ein alternativer Anbieter aufgesucht werden
- kann, im Fediverse, die gesamte Instanz blockiert werden

<b class="firstpart">Rollenspiel</b><b class="lastpart">Monster</b> versucht nicht, den Austausch zu zentralisieren, und du wirst viele andere Hosting-Provider finden, die ähnliche Dienste anbieten, mit Bedingungen, die dir vielleicht besser passen. Sollten dir die von <b class="firstpart">Rollenspiel</b><b class="lastpart">Monster</b> vorgeschlagenen Bedingungen nicht zusagen oder möchtest du dich einfach woanders registrieren, werden wir versuchen, dir einen möglichen Wechsel zu erleichtern (im Rahmen unserer Möglichkeiten und der verwendeten technischen Lösungen).

## Entwicklung der Charta

<b class="firstpart">Rollenspiel</b><b class="lastpart">Monster</b> behält sich das Recht vor, diese Charta gelegentlich zu aktualisieren und zu ändern.
