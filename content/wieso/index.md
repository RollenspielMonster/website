---
title: Wofür die Alternativen?
date: 2021-05-19T08:59:30.498Z
lastmod: 2023-02-03T18:51:41.202Z
tags:
  - ""
categories:
  - ""
slug: die-w-fragen
---

Wie damals in der [Sesamstraße](https://de.wikipedia.org/wiki/Sesamstra%C3%9Fe) das Titellied schon fragte, will ich euch über die W Fragen etwas über mich und das Projekt zu erzählen:

## Wer

Betrieben wird das Ganze von mir [Tealk](https://keyoxide.org/hkp/d6a42fb0a007bd7af8fd67145fe349abc863e7f9), mit dem Ziel zwei Hobbys bzw. Anliegen zu verbinden: Rollenspiel und Datenschutz. Begleitet wird er dabei vom süßen kleinen [Pino](/pino/).

## Wie

Indem wir eine Sammlung an verschiedenen dezentralen Diensten und Tools, die eine Ausweichmöglichkeit zu den bekannten Social Media Anbietern darstellen, anbieten. Die Hürden sollen so niedrig wie möglich gehalten werden. Die meisten dieser Dienste entstammen dem Fediverse, was das genau ist, erfährt man sehr gut auf [Wikipedia](https://de.wikipedia.org/wiki/Fediverse) oder [Digitalcourage](https://digitalcourage.de/digitale-selbstverteidigung/fediverse).

## Was

Das ist schwer zu beantworten, da die Welt nicht stillsteht und die digitale Welt sich noch ein gutes Stück schneller dreht. Aber im Allgemeinen alle Dienste, die man für das Thema Rollenspiel gebrauchen kann. Von Kommunikation bis zum Abspeichern von Webseiten oder kürzen von Links.

## Wo

Aktuell werden die Dienste auf mehreren Servern betrieben, die alle bei Hetzner (Datacenter-Park Nürnberg) angemietet worden sind. Außer den produktiven Servern gibt es noch einen Server bei Netcup welcher für die Backups zuständig ist.
Außerdem verzichten wir darauf Drittanbieter, Werbung und Ähnliches einzubauen. Aber durch diesen Verzicht sind wir auch auf eure Hilfe angewiesen, mehr dazu findet ihr in dem Bereich [Unterstütze uns](/donate/).

Auch das "überall" so beliebte Cloudflare wird bei uns aus gutem [Grund](https://www.kuketz-blog.de/the-great-cloudwall-weshalb-cloudflare-ein-krebsgeschwuer-ist/) gemieden sowie andere CDN Anbieter.

### Umwelt

Sowohl [Hetzner](https://www.hetzner.com/de/unternehmen/umweltschutz/) als auch [Netcup](https://www.netcup.de/ueber-netcup/oekostrom.php) betreiben, laut ihren eigenen Angaben, ihre Server CO₂-neutral. Dies ist natürlich kein Datenschutz Vorteil, aber auch die Umwelt ein wichtiger Faktor, wenn wir unsere Dienste noch länger anbieten wollen ;)

## Wieso/Weshalb/Warum

Um eine selbstbestimmte, datenschutzfreundliche und leicht bedienbare Alternative zu [GAFAM](https://de.wikipedia.org/wiki/GAFAM) anzubieten.
Ziel von GAFAM ist es ihren Profit zu maximieren und das, indem sie dafür sorgen, dass wir möglichst viel Zeit auf ihren Plattformen verbringen. Denn das Hauptprodukt von ihnen sind WIR, ok genauer gesagt unsere Daten und damit unser Verhalten. Diese Daten über unser Verhalten werden genutzt um uns z.B. zu manipulieren oder eben verkauft, damit andere Unternehmen ebenfalls Einfluss auf uns nehmen können. Kunden sind unter anderem auch politische Parteien, insbesondere vor Wahlen.
Hier ein paar Beiträge dazu:

- [Überwachungskapitalismus in der Wikipedia](https://de.wikipedia.org/wiki/%C3%9Cberwachungskapitalismus)
- [3sat Doku: The Social Dilemma: Wie Google & Co. uns verkaufen](https://www.3sat.de/wissen/scobel/the-social-dilemma-wie-google-und-co-uns-verkaufen-102.html)
- [Jung & Live: Tilo jung im Gespräch mit Gert Scobel](https://invidious.snopyta.org/watch?v=7ZtbYvwgK-w&autoplay=0)

Um diese datenschutzfreundliche Umgebung zu schaffen, versuche ich alle möglichen Techniken auszuschöpfen, z.B. das Entfernen von veralteten SSL Protokollen und Cipher. Außerdem gehen wir sogar so weit, dass selbst alle Zugriffslogs des Webservers abgeschaltet werden sowie alle weiteren Logs, welche Daten über euch enthalten könnten. Die einzigen Daten, die ihr bei uns hinterlasst, sind die, die ihr selbst eingebt.

### Nichts zu verbergen

Oftmals hört man im Kontext des Themas Datenschutz / Privatsphäre bzw. (staatlicher) Überwachung das abgedroschene Argument »Ich habe doch nichts zu verbergen!«. Diese Aussage ist nicht nur relativ kurzsichtig, sondern auch gefährlich. [Schon geschichtlich betrachtet](https://www.heise.de/ct/ausgabe/2015-17-Editorial-Nichts-zu-verbergen-2755486.html) sollte es eigentlich nicht notwendig sein, diese Aussage ständig widerlegen zu müssen. Wer diesen Satz von jemandem hört, der sollte dieser Meinung entschieden widersprechen und argumentieren, weshalb Privatsphäre ein Menschenrecht ist.

Oder von [Leena Simon](https://leena.de/): [Nichts zu verbergen? Ein moderner Mythos und 12 Argumente dagegen](https://www.kuketz-blog.de/nichts-zu-verbergen-ein-moderner-mythos-und-12-argumente-dagegen/)

Oder auch [Edward Snowden](https://de.wikipedia.org/wiki/Edward_Snowden), ein Privatsphäre-Fürsprecher, sagt:

> Zu argumentieren, dass Sie keine Privatsphäre brauchen, weil Sie nichts zu verbergen haben, ist so, als würden Sie sagen, dass Sie keine Meinungsfreiheit brauchen, weil Sie nichts zu sagen haben.

- Wer nichts zu verbergen hat, braucht auch kein Bankgeheimnis
- Wer nichts zu verbergen hat, braucht auch kein Anwaltsgeheimnis
- Wer nichts zu verbergen hat, braucht auch kein Briefgeheimnis
- Wer nichts zu verbergen hat, braucht auch kein Wahlgeheimnis
- Wer nichts zu verbergen hat, braucht auch kein Beichtgeheimnis
- Wer nichts zu verbergen hat, braucht auch keine ärztliche Schweigepflicht
- Wer nichts zu verbergen hat, braucht auch keine Vorhänge

<p class="text-end">immer einen Blick wert -&gt;<a href="https://www.kuketz-blog.de/empfehlungsecke/#verbergen">Quelle</a></p>
