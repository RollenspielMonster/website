---
title: Unterstütze uns
date: 2020-09-01T00:00:00.000Z
lastmod: 2023-02-03T18:53:09.222Z
tags:
  - ""
categories:
  - ""
slug: donate
---

Unsere Services und auch die Webseite sind und sollen weiterhin unabhängig bleiben. Dafür benötigen wir deine Unterstützung. Wir verzichten bewusst auf Werbung und User-Tracking. Diese Unabhängigkeit hat ihren Preis, über den auch [Digitalcourage schon einmal geschrieben hat](https://digitalcourage.de/digitale-selbstverteidigung/digitale-dienste-auch-mal-bezahlen).

Wie wir aufgestellt sind, könnt ihr auf unserer [Hardware und Software Übersichtsseite](/hardware_software/) einsehen. Regelmäßigen Einnahmen helfen uns längerfristig planen zu können und hält diese Infrastruktur am Laufen.

## <i class="fa-solid fa-building-columns"></i> Überweisung

Eine Überweisung auf unser Konto ist uns die liebste Form, da sie keinerlei zusätzliche Gebühren kostet und datenschutzfreundlich ist. Falls ihr in der Liste unten erwähnt werden wollt, gebt mir bitte euer einverständnis z.B. im Verwendungszweck.

<p class="text-center mt-3">Ganz besonders freuen wir uns über Daueraufträge. Schon ein paar Euro im Monat helfen uns, längerfristig planen zu können.</p>

<div class="row row-cols-1 row-cols-xxl-2">
  <div class="d-flex p-0 mb-3">
    <div class="d-none d-sm-block me-3">
      <div id="qrCodeContainer"></div>
    </div>
    <div id="bankDataContainer"></div>
  </div>
  <form class="d-none d-sm-block" id="paymentForm">
    <div class="form-group mb-3">
      <label for="amount">Spendenbetrag:</label>
      <div class="input-group">
        <input type="number" class="form-control" id="amount" name="amount" min="1" max="100" value="5" required>
        <div class="input-group-text">€</div>
        <button type="button" class="btn btn-outline-secondary mx-1" onclick="setAmount(5)">5 €</button>
        <button type="button" class="btn btn-outline-secondary mx-1" onclick="setAmount(10)">10 €</button>
        <button type="button" class="btn btn-outline-secondary mx-1" onclick="setAmount(15)">15 €</button>
        <button type="button" class="btn btn-outline-secondary mx-1" onclick="setAmount(50)">50 €</button>
        <button type="button" class="btn btn-outline-secondary mx-1" onclick="setAmount(100)">100 €</button>
      </div>
    </div>
    <div class="form-floating mb-3">
      <input type="text" class="form-control" id="notiz" name="notiz" placeholder="Notiz">
      <label for="notiz">Notiz z.B. Nickname</label>
    </div>
  </form>
</div>

<div class="d-none d-sm-block">
  <div class="row row-cols-1 row-cols-md-2 mt-4">
    <div class="col text-center">
      Falls ihr BUNQ Kunde seid oder einen der folgenden Dienste nutzen wollt: iDEAL, Kreditkarte, Sofort(Klarna) und Banccontact, könnt ihr auch unser bunq.me nutzen.
      <p class="d-none d-sm-block">Einfach QR-Code Scannen oder anklicken.</p>
    </div>
    <div class="col d-flex align-items-center justify-content-center">
      <a href="https://www.bunq.me/RollenspielMonster/10/Freiwillige%20finanzielle%20Unterstützung" target="_blank" rel="noopener">
        <img class="d-none d-sm-block" src="bunqqr.png" alt="bunq QR Code">
      </a>
    </div>
  </div>
</div>
<div class="d-sm-none">
  Falls ihr BUNQ Kunde seid oder einen der folgenden Dienste nutzen wollt: iDEAL, Kreditkarte, Sofort(Klarna) und Banccontact, könnt ihr auch unser bunq.me nutzen.
  <a href="https://www.bunq.me/RollenspielMonster/10/Freiwillige%20finanzielle%20Unterstützung" target="_blank" rel="noopener">
    <div class="text-center">Link</div>
  </a>
</div>

## <i class="fa-brands fa-paypal"></i> PayPal

<div class="d-none d-sm-block">
  <div class="row row-cols-1 row-cols-md-2">
    <div class="col d-flex align-items-center justify-content-center">
      <a href="https://www.paypal.com/donate/?hosted_button_id=7N97FXW2BRJNY" target="_blank" rel="noopener"><img class="d-sm-block" src="paypalqr.png" alt="PayPal QR Code"></a>
    </div>
    <div class="col text-center">
  PayPal ist ein weit verbreitetes Online-Zahlungssystem, das es Nutzern ermöglicht, Transaktionen über das Internet abzuwickeln. Es bietet eine bequeme Möglichkeit, Zahlungen zu senden und zu empfangen, ohne sensible Finanzdaten bei jedem Kauf offenlegen zu müssen.<br>
  Allerdings gibt es Bedenken hinsichtlich des Datenschutzes. PayPal sammelt und speichert umfangreiche Informationen über Transaktionen, Konten und persönliche Daten.
  <p class="d-none d-sm-block">Falls ihr über PayPal spenden wollt, könnt ihr auch folgenden QR-Code scannen oder anklicken.</p>
    </div>
    <div class="col d-flex align-items-center justify-content-center d-sm-none">
      <a href="https://www.paypal.com/donate/?hosted_button_id=7N97FXW2BRJNY" target="_blank" rel="noopener">Link</a>
    </div>
  </div>
</div>
<div class="d-sm-none">
  PayPal ist ein weit verbreitetes Online-Zahlungssystem, das es Nutzern ermöglicht, Transaktionen über das Internet abzuwickeln. Es bietet eine bequeme Möglichkeit, Zahlungen zu senden und zu empfangen, ohne sensible Finanzdaten bei jedem Kauf offenlegen zu müssen.<br>
  Allerdings gibt es Bedenken hinsichtlich des Datenschutzes. PayPal sammelt und speichert umfangreiche Informationen über Transaktionen, Konten und persönliche Daten.
  <div class="text-center">
    <a href="https://www.paypal.com/donate/?hosted_button_id=7N97FXW2BRJNY" target="_blank" rel="noopener">Link</a>
  </div>
</div>


## <i class="fa-solid fa-money-bill-wave"></i> Bar

Eine Barspende ist ebenfalls möglich, gerne auch anonym ohne Absender. Einfach an diese Adresse senden: \
<p class="bot">
kcuB leinaD<br/>
2 .rtsnehcriK<br/>
nralsE 39629
</p>

## <i class="fa-solid fa-comment-nodes"></i> Weitersagen

Wenn dir das Projekt gefällt, dann teile es mit deinen Freunden, Bekannten und Mitmenschen. Nutze dafür soziale Netzwerke, Foren, E-Mails oder einfach die nächste Feier / Veranstaltung.

Hier findet ihr dazu eine Auswahl unserer [Logos](/logos)

> Finanzielle Zuwendungen in Form von Spenden sind freiwillig, nicht erstattbar und werden ordnungsgemäß als Einnahme versteuert.

## <i class="fa-solid fa-chart-column"></i> Finanzübersicht

Hier seht ihr welche Einnahmen und Ausgaben das Projekt hat. Stand {{< time.inline >}}{{ now | time.Format "02.01.2006 15:04" }}{{< /time.inline >}}

### Jahresübersicht

<canvas id="myChart"></canvas>

### Monatslog

{{< donate_table.inline >}}
  {{ partial "donate_table.html" . }}
{{< /donate_table.inline >}}