---
title: Support us
date: 2020-09-01T00:00:00.000Z
lastmod: 2023-02-03T18:53:09.222Z
tags:
  - ""
categories:
  - ""
slug: donate
---

Our services and also the website are and shall remain independent. Therefore we need your support. We deliberately refrain from advertising and user tracking. This independence has its price, which [Digitalcourage has already written about](https://digitalcourage.de/digitale-selbstverteidigung/digitale-dienste-auch-mal-bezahlen).

You can see how we are set up on our [hardware and software overview page](/hardware_software/). Regular income helps us to plan for the long term and keeps this infrastructure running.

## <i class="fa-solid fa-building-columns"></i> Bank transfer

A bank transfer to our account is our favorite way, because it does not cost any additional fees and is privacy friendly.If you want to be mentioned in the list below, please give me your consent e.g. in the reason for payment.

<p class="text-center mt-3">We are particularly pleased about standing orders.Just a few euros a month help us to plan for the longer term.</p>

<div class="container">
  <div class="row">
    <div class="col-7">
      <div class="d-flex p-2">
        <div class="">
          <div id="qrCodeContainer"></div>
        </div>
        <div class="ms-3 mt-2">
          <div id="bankDataContainer"></div>
        </div>
      </div>
    </div>
    <div class="col">
      <form id="paymentForm" class="row gx-3 gy-2 align-items-center">
        <div class="form-group mb-3">
          <label for="amount">Spendenbetrag:</label>
          <div class="input-group">
            <input type="number" class="form-control" id="amount" name="amount" min="1" max="100" value="5" required>
            <div class="input-group-text">€</div>
            <button type="button" class="btn btn-outline-secondary mx-1" onclick="setAmount(5)">5 €</button>
            <button type="button" class="btn btn-outline-secondary mx-1" onclick="setAmount(10)">10 €</button>
            <button type="button" class="btn btn-outline-secondary mx-1" onclick="setAmount(15)">15 €</button>
            <button type="button" class="btn btn-outline-secondary mx-1" onclick="setAmount(50)">50 €</button>
            <button type="button" class="btn btn-outline-secondary mx-1" onclick="setAmount(100)">100 €</button>
          </div>
        </div>
        <div class="form-floating mb-3">
          <input type="text" class="form-control" id="notiz" name="notiz" placeholder="Notiz">
          <label for="notiz">Notiz z.B. Nickname</label>
        </div>
      </form>
    </div>
  </div>
</div>

<div class="container text-center">
  <div class="row">
    <div class="col d-flex align-items-center justify-content-center">
      If you are a BUNQ customer or want to use one of the following services: iDEAL, credit card, Sofort(Klarna) and Banccontact, you can also use our bunq.me. Simply scan or click on the QR code.
    </div>
    <div class="col d-flex align-items-center justify-content-center">
      <p class="text-center">
        <a href="https://www.bunq.me/RollenspielMonster/10/Freiwillige%20finanzielle%20Unterstützung" target="_blank" rel="noopener"><img src="bunqqr.png" alt="bunq QR Code"></a>
      </p>
    </div>
  </div>
</div>

## <i class="fa-brands fa-paypal"></i> PayPal

<div class="container text-center">
  <div class="row">
    <div class="col d-flex align-items-center justify-content-center">
      <p class="text-center">
        <a href="https://www.paypal.com/donate/?hosted_button_id=7N97FXW2BRJNY" target="_blank" rel="noopener"><img src="paypalqr.png" alt="PayPal QR Code"></a>
      </p>
    </div>
    <div class="col">
PayPal is a widely used online payment system that allows users to make transactions over the Internet. It offers a convenient way to send and receive payments without having to disclose sensitive financial information with every purchase.

However, there are privacy concerns. PayPal collects and stores extensive information about transactions, accounts, and personal information.

If you want to donate through PayPal, you can also scan or click the following QR code.
    </div>
  </div>
</div>

## <i class="fa-solid fa-money-bill-wave"></i> Bar

A cash donation is also possible, gladly anonymous without a return address. Simply send it to this address: \
<p class="bot">
kcuB leinaD<br/>
2 .rtsnehcriK<br/>
nralsE 39629
</p>

## <i class="fa-solid fa-comment-nodes"></i> Share

If you like the project, share it with your friends, acquaintances and fellow men. Use social networks, forums, e-mails or simply the next party / event.

Here you can find a selection of our [logos](/logos)

> Financial contributions in the form of donations are voluntary, non-refundable and are duly taxed as income.

## <i class="fa-solid fa-chart-column"></i> Financial overview

Here you can see the income and expenses of the project. Status {{< time.inline >}}{{ now | time.Format "02.01.2006 15:04" }}{{< /time.inline >}}

### Annual overview

<canvas id="myChart"></canvas>

### Monthly log

{{< donate_table.inline >}}
  {{ partial "donate_table.html" . }}
{{< /donate_table.inline >}}