---
title: Hard- & Softwareinfo
date: 2021-06-17T09:20:09.196Z
lastmod: 2022-07-20T08:25:55.195Z
tags:
  - ""
categories:
  - ""
slug: hardware_software
---

## Homepage

Erstellt mit Hugo und ausgeliefert über Codeberg Pages ([script](https://codeberg.org/Tealk/Git-Hooks_examples/src/branch/master/pre-push%20git-branch%20hugo))

## Netzplan

![Netzwerübersicht](Netzwerk.jpg)

## Main Server (Hetzner)

- Serverart: Dedicated
- Standort: Datacenter-Park Falkenstein, Deutschland
- CPU: AMD Ryzen™ 9 5950X
- RAM: 128 GB DDR4 ECC
- Drive1: 2 x 3.84 TB NVMe SSD
- Drive2: 6 TB SATA Enterprise Hard Drive
- Anbindung: 1 GBit/s

### Software

- Debian
- Proxmox Virtual Environment

### Virtuelle Server

- Debian

nach Bedarf:

- nginx
- php
- MariaDB
- PostgreSQL
- redis
- Elasticsearch
- composer
- Docker
- Node.js

## Backup Server (Netcup)

- Serverart: Dedicated
- Standort: Datacenter-Park Falkenstein, Deutschland
- CPU: Intel Core i7-8700
- RAM: 64 GB DDR4
- Drive1: 2x HDD SATA 6,0 TB Enterprise
- Anbindung: 1 GBit/s

### Software

- Debian
- Proxmox Backup Server

## AbuseIPDB

<a href="https://www.abuseipdb.com/user/90603" title="AbuseIPDB is an IP address blacklist for webmasters and sysadmins to report IP addresses engaging in abusive behavior on their networks">
	<img src="https://www.abuseipdb.com/contributor/90603.svg" alt="AbuseIPDB Contributor Badge" style="width: 401px;">
</a>
