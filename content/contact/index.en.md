---
title: Contact
date: 2021-06-17T09:20:09.196Z
lastmod: 2023-02-03T18:51:37.082Z
tags:
  - ""
categories:
  - ""
slug: contact
---

Become part of the rollenspiel.monster community. On our Fediverse nodes you can interact with other rollenspiel.monster users, your admin, as well as with other free software fans from other servers.

## Get support

- <i class="fa-solid fa-bug"></i> Our [Bugtracker](https://tasks.rollenspiel.monster/projects/8/12#share-auth-token=LJMESdz9gZrdcT6hjR9Zz4VvgQIpVKMDDSy3HgaR), where you can see known issues and their status.
- <i class="fa-solid fa-code-pull-request"></i> In the [Service requests](https://tasks.rollenspiel.monster/projects/2/48#share-auth-token=NdVI55vb4qFZqIjwGq2xEKSx4HwP5b8xHmFbWiI0) you can see which services are still planned and their status. And you can apply for it [here](https://rollenspiel.cloud/apps/forms/s/ncaGz7GFpiXbfGrjofbxotMD).
- <i class="fa-solid fa-users"></i> [**Community**support](https://rollenspiel.forum/c/rm_support) in the public forum - share your questions there so that everyone can benefit.
- <i class="fa-solid fa-ticket"></i> [**Project**support](https://ticket.rollenspiel.monster) also available via <a class="bot">retsnom.leipsnellor@troppus</a>.

**If your request contains data that should not be publicly visible, please use the project support.**

## Stay up to date

You can find the latest information about the project on these two nodes

- [Lemmy](https://rollenspiel.forum/c/rm_news) [<i class="fa-solid fa-rss"></i>](https://rollenspiel.forum/feeds/c/rm_news.xml?sort=New)
- [Mastodon](https://rollenspiel.social/@rollenspielmonster) [<i class="fa-solid fa-rss"></i>](https://rollenspiel.social/@rollenspielmonster.rss)

## Ask Tealk (your admin)

Please only contact me directly if it concerns sensitive/personal data. Otherwise see [Get support](/en/contact/#get-support).

- <i class="fa-solid fa-envelope"></i> <a class="bot">retsnom.leipsnellor@klaet</a> <a href="https://keyoxide.org/hkp/d6a42fb0a007bd7af8fd67145fe349abc863e7f9"><i class="fa fa-gnupg"></i> key</a>
- <i class="fa fa-matrix-org"></i> [@tealk:roleplay.chat](https://matrix.to/#/@tealk:rollenspiel.chat)
- <i class="fa fa-mastodon"></i> [tealk@rollenspiel.spocial](https://rollenspiel.social/@Tealk)
- <i class="fa-brands fa-signal-messenger"></i> [Signal](https://signal.me/#eu/vN_4-0D3KtzK8q4VgqU9Pr5J-FRS2U94NsGES55wQSMViTOSvLRCf4N0oZ6GLULU)
- <span class="fa-stack" style="font-size: 0.5em;"><i class="fa-solid fa-comment fa-stack-2x"></i><i class="fa-solid fa-lock fa-stack-1x fa-inverse"></i></span> [Threema](https://threema.id/VPR77Y35)
