---
title: Kontakt
date: 2021-06-17T09:20:09.196Z
lastmod: 2023-02-03T18:51:37.082Z
tags:
  - ""
categories:
  - ""
slug: contact
---

Werde Teil der rollenspiel.monster Community. Auf unseren Fediverse Nodes kannst du dich mit anderen rollenspiel.monster Nutzern, deinem Admin, sowie mit anderen Fans von freier Software von anderen Servern austauschen.

## Support erhalten

- <i class="fa-solid fa-bug"></i> Unser [Bugtracker](https://tasks.rollenspiel.monster/projects/8/12#share-auth-token=LJMESdz9gZrdcT6hjR9Zz4VvgQIpVKMDDSy3HgaR), darin könnt ihr bereits bekannte Probleme sehen und deren Status.
- <i class="fa-solid fa-code-pull-request"></i> In den [Servicewünschen](https://tasks.rollenspiel.monster/projects/2/48#share-auth-token=NdVI55vb4qFZqIjwGq2xEKSx4HwP5b8xHmFbWiI0) könnt ihr sehen welche Services noch geplant sind sowie deren Status. Und [hier](https://rollenspiel.cloud/apps/forms/s/ncaGz7GFpiXbfGrjofbxotMD) könnt ihr sie beantragen.
- <i class="fa-solid fa-users"></i> [**Community**support](https://rollenspiel.forum/c/rm_support) im öffentlichen Forum – teilt eure Fragen dort, damit alle davon profitieren können.
- <i class="fa-solid fa-ticket"></i> [**Projekt**support](https://ticket.rollenspiel.monster) auch über <a class="bot">retsnom.leipsnellor@troppus</a> erreichbar.

**Falls eure Anfrage Daten enthält, die nicht öffentlich einsehbar sein sollen, nutzt bitte den Projektsupport.**

## Auf dem laufenden Bleiben

Auf diesen zwei Nodes findest du die neusten Informationen über das Projekt

- [Lemmy](https://rollenspiel.forum/c/rm_news) [<i class="fa-solid fa-rss"></i>](https://rollenspiel.forum/feeds/c/rm_news.xml?sort=New)
- [Mastodon](https://rollenspiel.social/@rollenspielmonster) [<i class="fa-solid fa-rss"></i>](https://rollenspiel.social/@rollenspielmonster.rss)

## Tealk fragen (dein Admin)

Bitte kontaktiert mich nur direkt wenn es um heikle/persönliche Daten geht. Ansonsten siehe [Support erhalten](/contact/#support-erhalten).

- <i class="fa-solid fa-envelope"></i> <a class="bot">retsnom.leipsnellor@klaet</a> <a href="https://keyoxide.org/hkp/d6a42fb0a007bd7af8fd67145fe349abc863e7f9"><i class="fa fa-gnupg"></i>-Schlüssel</a>
- <i class="fa fa-matrix-org"></i> [@tealk:rollenspiel.chat](https://matrix.to/#/@tealk:rollenspiel.chat)
- <i class="fa fa-mastodon"></i> [tealk@rollenspiel.spocial](https://rollenspiel.social/@Tealk)
- <i class="fa-brands fa-signal-messenger"></i> [Signal](https://signal.me/#eu/vN_4-0D3KtzK8q4VgqU9Pr5J-FRS2U94NsGES55wQSMViTOSvLRCf4N0oZ6GLULU)
- <span class="fa-stack" style="font-size: 0.5em;"><i class="fa-solid fa-comment fa-stack-2x"></i><i class="fa-solid fa-lock fa-stack-1x fa-inverse"></i></span> [Threema](https://threema.id/VPR77Y35)
